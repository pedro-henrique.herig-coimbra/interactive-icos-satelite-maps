import numpy as np

from scripts.common import sum_nan_arrays, metadata
from scripts.satelite import cut_proj, crop_tif_by_polygon, plot_sat, get_path_to_satelite, get_sat_data
from rasterio import open as rastopen
from rasterio.plot import plotting_extent
from scripts.surfmodel import vprm
from scipy.ndimage import zoom
from pandas import to_datetime, Timedelta
from os import path

"""Meta for Satelite Products"""
satproducts_dic = {
    "NDVI": {
        'formula': "(B8-B4)/(B8+B4)",
        'filename': "/NDVI.tif",
        'reqbands': ['B4', 'B8']},
    "EVI": {
        'formula': "2.5*(B8-B4)/(B8+6*B4-7.5*B2+1)",
        'filename': "/EVI.tif",
        'reqbands': ['B2', 'B4', 'B8']},
    "LSWI": {
        'formula': "(B8-B11)/(B8+B11)",
        'filename': "/LSWI.tif",
        'reqbands': ['B8', 'B11']},
    "RGB": {
        'formula': "composite(B2, B3, B4)",
        'filename': "/RGB.tif",
        'reqbands': ['B2', 'B3', 'B4']}
        }

class satprod:
    def __init__(self, **kwargs):
        "satelite products"        
        self.meta = {}
        self.date = None
        self.data = None
        self.crs = 3035
        self.dx = None
        self.dy = None
        self.extent = None
        self.__dict__.update(**kwargs)
    
    def select(self, select):
        result = satprod(**vars(self))
        result.date = result.date[select] if result.date is not None else None
        result.data = np.array([result.data[select]]) if result.data is not None else None
        return result

    def get(self, product, filepath=None, **kwargs):
        self.name = product
        if product.upper() in ["LANDUSE", "LAND USE", "LU"]:
            """
            https://theia.cnes.fr/atdistrib/rocket/#/collections/OSO/2327b748-a82c-5933-afb0-087bbfeff4cd
            "../eddy-tower_largefiles/data/input/raw/sat/OSO_20200101_RASTER_V1-0/DATA/OCS_2020.tif"
            http://alapterkep.termeszetem.hu/
            https://envs.au.dk/faglige-omraader/samfund-miljoe-og-ressourcer/arealanvendelse-og-gis/basemap/basemap03-geotiff-til-download
            """
            landuse = cut_proj(filepath, **kwargs)
            self.data = landuse.read(1)
            self.extent = plotting_extent(landuse)
            self.profile = landuse.profile
            
        elif product.upper() in ["NDVI", "EVI", "LSWI", "RGB"]:
            self.data, self.extent, self.profile, _ = get_sat_product(
                method=product.upper(), **kwargs)

        if product.upper() == "VPRM":
            vprm_param = {}  #{'PAR': 1000, 'T': 5}
            vprm_param.update(**kwargs)
            if np.mean(vprm_param['T']) > 200: vprm_param['T'] = list(np.array(vprm_param['T'])-273.15)  # K-> C
            
            EVI = kwargs['EVI'] if "EVI" in kwargs.keys() else satprod().get("EVI", **kwargs, **self.meta)
            EVI.data[(EVI.data<0) | (EVI.data>1)] = np.nan
            vprm_param['EVI'] = EVI.data

            LSWI = kwargs['LSWI'] if "LSWI" in kwargs.keys() else satprod().get("LSWI", **kwargs, **self.meta)
            LSWI.data[(LSWI.data<-1) | (LSWI.data>1)] = np.nan
            vprm_param['LSWI'] = LSWI.data

            self.data, self.gpp, self.reco_a, self.reco_b = vprm(**vprm_param)
            
            self.extent = EVI.extent
            self.profile = EVI.profile
            self.dx = EVI.dx
            self.dy = EVI.dy
            self.xaxis = EVI.xaxis
            self.yaxis = EVI.yaxis
        
        else:
            self.dx = self.profile['transform'][0]
            self.dy = self.profile['transform'][4]

            """
            x ....       ......
            :    :   =>  :    :
            ......       x ....
            """
            if self.dy < 0:
                self.dy = abs(self.dy)
                self.data = np.flip(self.data, axis=-2)
            if self.dx < 0:
                self.dx = abs(self.dx)
                self.data = np.flip(self.data, axis=-1)

            self.xaxis = np.arange(0, self.data.shape[-2]) * self.dx + self.profile['transform'][2]
            self.yaxis = np.arange(0, self.data.shape[-1]) * self.dy + self.profile['transform'][5]
            # self.dx = abs(self.extent[1] - self.extent[0]) / (self.data.shape[-1])
            # self.dy = abs(self.extent[3] - self.extent[2]) / (self.data.shape[-2])
        return self

    def crop(self, boundarypolygon, **kwargs):
        #crop_extent = self.extent[:2] + self.extent[3] + self.extent[2]
        cropped = satprod(**vars(self))
        if len(self.data.shape) == 4:
            cropped.data = np.array([[crop_tif_by_polygon(
                cutshape=boundarypolygon, basegrid=bg, extent=self.extent) for bg in bgband] for bgband in self.data])
        if len(self.data.shape) == 3:
            cropped.data = np.array([crop_tif_by_polygon(
                cutshape=boundarypolygon, basegrid=bg, extent=self.extent) for bg in self.data])
        if len(self.data.shape) == 2:
            cropped.data = crop_tif_by_polygon(cutshape=boundarypolygon,
                                                basegrid=self.data, extent=self.extent)
        return cropped
    
    def plot(self, index=0, data=None, mask=None, imask=None, **kwargs):  # origin="upper",
        if data is None:
            data = [self.data] #[self.xaxis, self.yaxis, self.data]
        else:
            data = [data]
        if len(data[-1].shape) > 2:
            data[-1] = data[-1][index]
        
        if mask is not None:
            data[-1] = np.ma.array(data[-1], mask=data[-1] == mask)
        if imask is not None:
            data[-1] = np.ma.array(data[-1], mask=data[-1] != imask)
        
        plot_sat(*data,
                 extent=self.extent, **kwargs)
        return


"""
NDVI, EVI, LSWI
use a generator to call function get_ndvi:
e.g.: (get_ndvi(time) for time in timelist)
"""
'''
def get_ndvi(**kwargs):
    return __sat_product_i__(method="NDVI", **kwargs)


def get_evi(**kwargs):
    return __sat_product_i__(method="EVI", **kwargs)


def get_lswi(**kwargs):
    return __sat_product_i__(method="LSWI", **kwargs)
'''


def linear_interpolate_maps(product_dic, quality):
    print('Interpolating map...')
    def pad(data):
        good = np.isfinite(data)
        interpolated = np.interp(np.arange(data.shape[0]),
                                 np.flatnonzero(good),
                                 data[good])
        return interpolated

    for time in product_dic.keys():
        """prepare each time"""
        if float(quality[time]) <= 1:
            """if current date is on the same day as the satelite, nan to zero so not to interpolate"""
            product_dic[time][np.isnan(product_dic[time])] = 0
        else:
            """if current date is not on the same day as the satelite, nan so to interpolate"""
            product_dic[time] = product_dic[time] * np.nan


    product_dic = {k: v for k, v in zip(product_dic.keys(), np.apply_along_axis(
        pad, 0, np.array(list(product_dic.values()))))}
    
    return product_dic


def get_sat_product(timelist, interpolate="linear", **kwargs):
    meta_dic = {}
    product_dic = {}
    product_lst = []
    
    if isinstance(timelist, list)==False:
        timelist = [timelist]
        interpolate = None  # "closest"
    
    for time in np.unique([j.date() for j in timelist]): 
        """run only for different days"""
        time_ = to_datetime(str(time) + " 12:00:00")

        sat_foldername, sat_date, sat_date_dif = get_path_to_satelite(
            date=time_, **kwargs)

        if sat_foldername == sat_date == sat_date_dif == None:
            return np.array([[np.nan]]), [np.nan]*4, {}

        product = __sat_product_i__(
            sat_foldername, sat_date, sat_date_dif, returnTIF= False, ** kwargs)
        product_dic[str(time)] = product['data']       
        meta_dic[str(time)] = product['meta'].select(['datedif'])
    
    if interpolate=="linear":
        """if > 1 day available, linear interpolation of each pixel"""
        product_dic = linear_interpolate_maps(
            product_dic, {el: meta_dic[el].datedif for el in meta_dic.keys()})
    
    for time in timelist:
        """attribute daily satelite data to each 30 min"""
        product_lst = product_lst + [product_dic[str(time.date())]]
    
    #product_lst = {'data': np.array(product_lst), "extent": product['extent'], "meta": meta_dic}
    return np.array(product_lst), product['extent'], product['profile'], meta_dic


def force_array_dimension(shape, out):
    """
    Return array with same shape as base one.
    """
    out_ = np.zeros(shape) * np.nan

    shape_dif = (np.array(shape) - np.array(out.shape)) / 2
    signal = shape_dif / abs(shape_dif)

    bas_cut = [None] * 4
    out_cut = [None] * 4

    for i, s_ in enumerate(signal):

        dif = [int(np.ceil(abs(shape_dif[i]))), -
               int(np.floor(abs(shape_dif[i])))]
        dif = [el if el != 0 else None for el in dif]

        if i == 0:
            if s_ == 1:
                bas_cut[:2] = dif

            elif s_ == -1:
                out_cut[:2] = dif

        elif i == 1:
            if s_ == 1:
                bas_cut[2:] = dif

            elif s_ == -1:
                out_cut[2:] = dif

    out_[bas_cut[0]:bas_cut[1],
         bas_cut[2]:bas_cut[3]] = \
        sum_nan_arrays(out_[bas_cut[0]:bas_cut[1],
                               bas_cut[2]:bas_cut[3]],
                          out[out_cut[0]:out_cut[1],
                              out_cut[2]:out_cut[3]])

    return out_



def __sat_product_i__(sat_foldername, sat_date, sat_date_dif, center, buffer=600, returnTIF=True, method="NDVI", **kwargs):
    satprod_meta = metadata(**satproducts_dic[method])

    sat_data, satdata_meta = get_sat_data(sat_date, center=center, sat_foldername=sat_foldername, 
                                  sat_date_dif=sat_date_dif,
                                  bands=satprod_meta.reqbands, buffer=buffer,
                                  plot=False, force=False, **kwargs)
    band = satdata_meta.bands
    satprod_meta.update(**vars(satdata_meta))

    sat_date = path.split(sat_data[1].name)[0].split('/')[-1]

    # Preparing metadata
    prod_path = "data/tmp/SENTINEL2/" + sat_date + satprod_meta.filename
    prod_metapath = path.splitext(prod_path)[0] + ".metadata"

    satprod_meta.filepath = prod_metapath
    
    same_metadata = path.exists(prod_path) and \
        False #satprod_meta.check(attrs=['formula', 'datedif', 'ref', 'tile', 'buffer', 'center'])
        
    if not same_metadata:
        if method=="NDVI":
            """
            NDVI = (NIR - Red) / (NIR + Red), 
            recommended NIR=B8A but its resolution is lower than B8
            """
            prod_data = (sat_data[band['B8']].read(1) - sat_data[band['B4']].read(1)) / (sat_data[band['B8']].read(1) + sat_data[band['B4']].read(1))
            #prod_data[(prod_data<-1) | (prod_data>1)] = np.nan
                
        if method=="EVI":
            """
            EVI = 2.5 * (NIR - Red) / (NIR + (6 * Red - 7.5 * Blue) + 1)
            """
            prod_data = 2.5 * (sat_data[band['B8']].read(1) - sat_data[band['B4']].read(1)) / (sat_data[band['B8']].read(1) + 6 * sat_data[band['B4']].read(1) - 7.5 * sat_data[band['B2']].read(1) + 1*10000)
            prod_data[(prod_data<0) | (prod_data>1)] = np.nan
            #prod_data = np.clip(prod_data, 0, 1)
                
        if method == "LSWI":
            """
            LSWI = (NIR - SWIR) / (NIR + SWIR)
            SWIR is the shortwave infrared (1.58–1.75 μm), 
            in sentinel 2 for SWIR we have B11 and B12 both at 20 m resolution,
            so we need to pass band 11 from 20 m to 10 m (1 pixel -> 4 equal pixels)
            """
            shp_11 = np.array(plotting_extent(sat_data[band['B11']]))
            shp_8 = np.array(plotting_extent(sat_data[band['B8']]))
            corr_20m_to_10m = np.round((shp_8-shp_11)/10, 0).astype(int)
            corr_20m_to_10m = [c if c!=0 else None for c in corr_20m_to_10m]
            sat_data_B11 = zoom(sat_data[band['B11']].read(1),[2,2],order=0,mode='nearest')
            sat_data_B11 = np.array([el[corr_20m_to_10m[0]:corr_20m_to_10m[1]] for el in sat_data_B11[corr_20m_to_10m[2]:corr_20m_to_10m[3]]])
            
            if sat_data[band['B8']].read(1).shape != sat_data_B11.shape:
                sat_data_B11 = force_array_dimension(sat_data[band['B8']].read(1),
                                                     sat_data_B11)
                """shape_dif = (np.array(sat_data[band['B8']].read(
                    1).shape) - np.array(sat_data_B11.shape)) / 2

                shape_dif = [int(np.ceil(shape_dif[0])), -int(np.floor(shape_dif[0])),
                             int(np.ceil(shape_dif[1])), -int(np.floor(shape_dif[1]))]
                shape_dif = [el if el != 0 else None for el in shape_dif]

                sat_data_B11_z = np.zeros(
                    sat_data[band['B8']].read(1).shape)  # * np.nan

                sat_data_B11_z[shape_dif[0]:shape_dif[1],
                               shape_dif[2]:shape_dif[3]] += sat_data_B11
                sat_data_B11 = sat_data_B11_z"""

            prod_data = (sat_data[band['B8']].read(1) - sat_data_B11) / (sat_data[band['B8']].read(1) + sat_data_B11)
            prod_data[(prod_data < -1) | (prod_data > 1)] = np.nan
            #prod_data = np.clip(prod_data, -1, 1)
                
        if method=="RGB":
            """
            Recreating true color image
            """
            prod_data = np.array([sat_data[band[b]].read(1) for b in ['B2', 'B3', 'B4']])
            prod_data = prod_data[(2, 1, 0), :, :]
            prod_data = (prod_data - prod_data.min()) * \
                (float(255 - 0) / (prod_data.max()-prod_data.min())) + 0
            prod_data = (prod_data.clip(0, 255) + 0.5).astype(np.uint8)
                
        prod_meta = sat_data[band[satprod_meta.reqbands[0]]].profile

        if method=="RGB": 
            prod_meta.update(**{'dtype': np.uint8, "count": 3})
        else: 
            prod_meta.update(**{'dtype': 'float64', "nodata": np.nan})

        prod_meta.update(
            **{'datedif': satprod_meta.datedif/Timedelta(1, 'd')})
        
        satprod_meta.update(**prod_meta)

        with rastopen(prod_path, "w", **prod_meta) as dest:
            if method=="RGB": dest.write(np.array(prod_data))
            else: dest.write(np.array(list([prod_data])))
        
        satprod_meta.write()
        
    with rastopen(prod_path, "r") as prod_file:
        if returnTIF:
            return(prod_file)
        else:
            if method=="RGB": prod_data = prod_file.read()
            else: prod_data = prod_file.read(1)
            product = {"data": prod_data,
                       "extent": plotting_extent(prod_file),
                       "profile": prod_file.profile,
                       "meta": satprod_meta}
            
    return product

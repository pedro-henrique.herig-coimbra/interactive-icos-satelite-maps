from numpy import expand_dims, array, nanmax, exp

def vprm(EVI, LSWI, PAR, T, PAR0, Tlow, Tmax, Tmin, Topt, α, β, λ, lu_map=None, lu_dic=None, verbosity=0, **kwargs):
    """ 
    Mahadevan et al., 2008; 
    Xiao et al., 2004; 
    Yuan et al., 2015 
    """
    T = expand_dims(array(T), axis=(1, 2))
    PAR = expand_dims(array(PAR), axis=(1, 2))
    Tmin = array([Tmin]*len(T))
    Tmax = array([Tmax]*len(T))
    Topt = array([Topt]*len(T))
    #λ = array([λ]*len(T))
    #PAR0 = array([PAR0]*len(T))
    #α = array([α]*len(T))
    #β = array([β]*len(T))

    Tscale = ((T - Tmin) * (T - Tmax)) / \
        (((T - Tmin) * (T - Tmax)) - (T - Topt)**2)
    # Pscalar = (1 + LSWI)/2 [During bud burst to leaf full expansion] and = 1 [After leaf full expansion]
    Pscale = (1 + LSWI) / 2
    Wscale = (1 + LSWI) / (1 + nanmax(LSWI))
    
    fgpp_ls = - (λ * Tscale * Pscale * Wscale) * \
        (1 / (1 + PAR/PAR0)) * EVI * PAR
    reco_exp_t = exp(4.6 * (1/40 - 1/T))
    reco_exp_t[T[:] < 0] = 0
    reco_ls_a = α * T #* reco_exp_t
    reco_ls_b = β
    fco2_ls = fgpp_ls + reco_ls_a + reco_ls_b
    return fco2_ls, fgpp_ls, reco_ls_a, reco_ls_b


def vprm_(*args, **kwargs): 
    return vprm(*args, **kwargs)[0]


def save_meshgrid_tif(dat, profile):
    # save it so that I can plot histogram
    return

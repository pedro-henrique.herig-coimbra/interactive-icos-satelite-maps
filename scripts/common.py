"""
Common functions
"""

from sys import modules
from os import path
from re import search
from pickle import load, dump
from copy import deepcopy
from numpy import isnan, where, nan, array, searchsorted, isfinite, mod, pi, arctan2, zeros, matrix, linspace
from numpy.ma import array as marray

import pandas as pd
from itertools import chain

from folium import Figure as fFigure, Map as fMap, Marker as fMarker, TileLayer, FeatureGroup, Polygon
from folium import map as fmap
from folium.plugins import Fullscreen as fFullscreen
from folium.plugins import BeautifyIcon

from matplotlib import cm

from shapely.geometry import Polygon as gPolygon, Point as gPoint
from geopandas import GeoDataFrame, read_file as geo_read_file
from geopandas.io import file as gpd_io_file
from shapely import ops
from pyproj import Transformer

from matplotlib.colors import to_hex 
            

class LazyCallable(object):
  def __init__(self, name):
    self.n, self.f = name, None
  def __call__(self, *a, **k):
    if self.f is None:
      modn, funcn = self.n.rsplit('.', 1)
      if modn not in modules:
        __import__(modn)
      self.f = getattr(modules[modn],
                       funcn)
    self.f(*a, **k)


def nearest(items, pivot, direction=0):
    if direction == 0:
        nearest = min(items, key=lambda x: abs(x - pivot))
        difference = abs(nearest - pivot)
        
    elif direction == -1:
        nearest = min(items, key=lambda x: abs(x - pivot) if x<pivot else pd.Timedelta(999, "d"))
        difference = (nearest - pivot)
        
    elif direction == 1:
        nearest = min(items, key=lambda x: abs(x - pivot) if x>pivot else pd.Timedelta(999, "d"))
        difference = (nearest - pivot)
    
    return nearest, difference


def sum_nan_arrays(a, b):
    ma = isnan(a)
    mb = isnan(b)
    return where(ma & mb, nan, where(ma, 0, a) + where(mb, 0, b))


def prioritize_list(list, renames):
    match = [search(renames, el) for el in list]
    match = [el != None for el in match]
    match = array(list)[match]
    match = [list.index(el) for el in match]
    for el in match:
        list.insert(0, list.pop(el))
    return list

def replace_with_dict(ar, dic):
    # Extract out keys and values
    k = array(list(dic.keys()))
    v = array(list(dic.values()))

    # Get argsort indices
    sidx = k.argsort()
    
    # Drop the magic bomb with searchsorted to get the corresponding
    # places for a in keys (using sorter since a is not necessarily sorted).
    # Then trace it back to original order with indexing into sidx
    # Finally index into values for desired output.
    return v[sidx[searchsorted(k,ar,sorter=sidx)]]
    
class metadata():
    def __init__(self, filepath=None, **kwargs):
        self.filepath = filepath
        if kwargs:
            self.update(**kwargs)
        #self.__dict__.update(kwargs)

    def update(self, **kwargs):
        kwargs = {k: v.to_dict() if isinstance(v, pd.DataFrame) else v for k, v in kwargs.items()}
        self.__dict__.update(kwargs)
        return

    def select(self, attrs: list):
        return metadata(**{k: vars(self).pop(k, None) for k in attrs})

    def check(self, attrs=None):
        if path.exists(self.filepath):
            log = load(open(self.filepath, 'rb'))
            if attrs:
                
                check = metadata(**vars(self))
                return {k: vars(check).pop(k, None) for k in attrs} == {
                    k: vars(log).pop(k, None) for k in attrs}
                """
                return [vars(self)[k] == vars(log)[k] for k in attrs \
                    if k in vars(self).keys() and k in vars(log).keys()]
                """
            else:
                return vars(self) == vars(log)
        else:
            return False

    def write(self):
        dump(self, open(self.filepath, 'wb'))
        return

    def print_file(self):
        if path.exists(self.filepath):
            log = load(open(self.filepath, 'rb'))
            print('\n'.join("%s: %s" % item for item in vars(log).items()))
        else:
            print('No logfile found.')
        return

    def print(self):
        print('\n'.join("%s: %s" % item for item in vars(self).items()))
        return


class ECnetwork():
    def __init__(self):
        stations = pd.read_excel(
            "data/stations.xlsx")
        stations.columns = map(str.lower, stations.columns)
        self.name = stations["name"].to_list()
        self.__dict__.update(**stations.set_index('name').to_dict())
        '''self.loc = {}
        for st in self.name:
            if isfinite(self.Latitude[st] + self.Longitude[st]):
                """get site location"""
                site_loc = [self.Longitude[st]] + [self.Latitude[st]]
                latlon = ops.transform(Transformer.from_crs("EPSG:4326", "EPSG:3035", always_xy=True).transform,
                                    gPoint((site_loc)))  # (y, x)
                self.loc.update({st: list(latlon.coords[0])})'''
        return
    
    def plot(self, zoom_start=4, **kwargs):
        ECmap = fFigure(height=300)
        ECmap = fMap(location=[48, 8], zoom_start=zoom_start,
                           #scrollWheelZoom=False,
                           **kwargs).add_to(ECmap)

        for st in self.name:
            if isfinite(self.latitude[st] + self.longitude[st]):
                fMarker([self.latitude[st], self.longitude[st]],
                              tooltip=st + '<br>' + '<br>'.join("%s: %s" % item for item in {k: vars(self)[k][st] for k in ['project', 'country', 'latitude', 'longitude', 'tile'] if k in vars(self).keys()}.items())).add_to(ECmap)

                #fMarker([site.geometry.y, site.geometry.x],
                #              tooltip=st).add_to(ECmap)
        return ECmap


class ECsite():
    def __init__(self, SiteName):
        self.name = SiteName
        
        stations = pd.read_excel(
            "data/stations.xlsx")
        stations.columns = map(str.lower, stations.columns)
        # screen site name
        stations = stations[(stations["name"] == SiteName)]
        attrs = stations.set_index('name').to_dict()
        attrs = {el: attrs[el][SiteName] for el in attrs.keys()}
        self.__dict__.update(**attrs)

        # get site location
        site_loc = list(stations["longitude"].values) + \
            list(stations["latitude"].values)
        latlon = ops.transform(Transformer.from_crs("EPSG:4326", "EPSG:3035", always_xy=True).transform,
                               gPoint((site_loc)))  # (y, x)
        self.loc = list(latlon.coords[0])

        # get site tile
        if pd.isnull(stations.satelitetile).any():
            stations.loc[pd.null(stations.satelitetile), 'satelitetile'] = add_sentinel_tile_in_dataframe(
                stations[pd.null(stations.satelitetile)])
        self.tile = stations.satelitetile.to_list()[0]

        if self.targetareafilepath not in [None, nan]:
            if path.isfile(self.targetareafilepath):
                self.targetarea = geo_read_file(
                    self.targetareafilepath, driver='KML')
                self.targetarea = self.targetarea.to_crs("EPSG:3035")
                self.targetarea = gPolygon(self.targetarea.geometry[0])
            else:
                self.targetarea = 'Err: NotFound'
        else:
            self.targetarea = nan
        
        luinfo = pd.read_excel(
            "data/landuse.xlsx", index_col='CO')
        luinfo.columns = map(str.lower, luinfo.columns)
        luinfo = luinfo.to_dict()
        self.lu_path = luinfo['landusefilepath'][self.co]
        self.lu_metapath = luinfo['landusemetafilepath'][self.co]
        self.lu_resolution = luinfo['resolution'][self.co]
        
    def get(self):
        print('\n'.join("%s: %s" % item for item in vars(self).items()))

    def print(self):
        print('\n'.join("%s: %s" % item for item in vars(self).items()))

    def plot(self, **kwargs):
        if isfinite(self.latitude + self.longitude):
            ECmap = fFigure(height=300)
            ECmap = fMap(location=[self.latitude, self.longitude],
                               #zoom_start=8, scrollWheelZoom=False,
                               **kwargs).add_to(ECmap)

            fMarker([self.latitude, self.longitude],
                          tooltip='<br>'.join("%s: %s" % item for item in {k: vars(self)[k] for k in ['name', 'country', 'latitude', 'longitude', 'tile']}.items())).add_to(ECmap)
        return ECmap


class parameter_model:
    def __init__(self, params=None, subclasses=None, **kwargs):
        assert (not params) or sum(p_ in set(kwargs.keys())
                                for p_ in set(params)) == len(params), "Parameters not declared."
        assert (not subclasses) or sum(set(subclasses) == set(kwargs[p_].keys()) 
                                  for p_ in params) == len(params), "Parameters' subclasses not declared."
        kwargs = deepcopy(kwargs)
        
        if params:
            self.params = list(params)
            self.subclasses = list(subclasses) if subclasses else ['value']
            self.dimension = 2 if subclasses else 1
            pkwargs = {k: kwargs.pop(k, nan)
                    for k in params}
            pkwargs = {k: {v: pkwargs[k].pop(v) for v in subclasses} if subclasses else {'value': pkwargs.pop(k)}
                    for k in params}
        else:
            self.params = None
            self.subclasses = None
            self.dimension = None
            pkwargs = {}

        self.__dict__.update(**pkwargs, **kwargs)


    def extract(self):
        return {p_: vars(self)[p_] for p_ in self.params}

    def pop(self, out, p_out=None, sc_out=None):
        assert (out in self.params) != (out in self.subclasses), \
            'Umbiguous, value is in parameters and subclasses.'
        p_out = out if out in self.params else None
        sc_out = out if out in self.subclasses else None

        if p_out and sc_out is None:
            self.params.pop(self.params.index(
                    p_out)) if p_out in self.params else None
            self.__dict__.pop(p_out, None)
        if p_out is None and sc_out:
            self.subclasses.pop(self.subclasses.index(
                sc_out)) if sc_out in self.subclasses else None
            [self.__dict__[p_].pop(sc_out, None) for p_ in self.params]
        return self

    def get(self, param=None, subclass=None):
        if subclass and param:
                return vars(self)[param][subclass]
        elif subclass:
            return {p_: vars(self)[p_][subclass] for p_ in self.params}
        elif param:
            return {subclass: vars(self)[param][sc_] for sc_ in self.subclasses}
        else:
            return {p_: {sc_: vars(self)[p_][sc_] for sc_ in self.subclasses} for p_ in self.params}

    def update_subclass(self, name, value):
        assert set(value.keys()) == set(self.params), "Parameters for new subclass do not match previous data."
        self.subclasses += [name]
        for p_ in self.params:
            self.__dict__[p_].update({name: value[p_]})
        return self

    def update_parameter(self, value: dict):
        if self.dimension == 1:
            value = {k: {'value': v} for k, v in value.items()}
        for p_ in list(set(self.params) & set(value.keys())):
            for sc_ in list(set(self.subclasses) & set(value[p_].keys())):
                self.__dict__[p_].update({sc_: value[p_][sc_]})
        return self

    def map(self, map, dic):
        assert len(map.shape) == 2, "Map is not 3D (shape: " + str(map.shape) + ').'
        result = {}
        for p_ in self.params:
            result[p_] = zeros(map.shape).astype('float32') * nan
            for lu_ in list(set(self.subclasses) & set(dic.keys())):
                inlu = sum([marray(map, mask=map == el).astype('float32')
                            for el in dic[lu_]])
                inlu[~inlu.mask] = nan
                inlu[inlu.mask] = 1
                map_ = vars(self)[p_][lu_] * inlu
                #map_ = array(
                #    [[vars(self)[p_][lu_] if el in dic[lu_] else nan for el in line][:] for line in map])
                result[p_] = sum_nan_arrays(result[p_], map_)
            #result[p_][:][result[p_] == 0] = nan
        return result

    def print(self):
        print('\n'.join("%s: %s" % item for item in vars(self).items()))
        return


def wind_direction(u, v): 
    wd = mod(180 + (180/pi) * arctan2(v, u), 360)
    return wd
    

def add_sentinel_tile_in_dataframe(fcdf):
    # Get Sentinel-2 tile
    def tile_by_position(lon, lat):
        """Check if point inside tile (should add alert on the borders)"""
        for tile in sentinel2_map.itertuples():
            if tile.geometry.contains(gPoint(lon, lat)):
                return(tile.Name)

    gpd_io_file.fiona.drvsupport.supported_drivers['KML'] = 'rw'
    sentinel2_map = geo_read_file(
        "data/sat/SENTINEL2/S2A_OPER_GIP_TILPAR_MPC__20151209T095117_V20150622T000000_21000101T000000_B00.kml", driver='KML')

    fcdf['SateliteTile'] = fcdf.apply(
        lambda row: tile_by_position(row.Longitude, row.Latitude), 1)
    return fcdf


def icoswebsite2stations():
    ds_ec = pd.read_csv(
        "data/stations_icos_website.csv")
    ds_ec = ds_ec[['Id', 'Name', 'Country', 'Position']]
    ds_ec['Longitude'] = ds_ec.Position.str.split(
        expand=True)[0].apply(pd.to_numeric, errors='coerce')
    ds_ec['Latitude'] = ds_ec.Position.str.split(
        expand=True)[1].apply(pd.to_numeric, errors='coerce')
    
    # Get Country code
    ds_ec['CO'] = ds_ec.Country.str[-3:-1]

    # Get SiteCode
    ds_ec['SiteName'] = ds_ec.Id.str.extract(r'.+\/(.+)')
    ds_ec.loc[pd.isnull(ds_ec.SiteName), 'SiteName'] = ds_ec[pd.isnull(
        ds_ec.SiteName)].Id

    # Get Sentinel-2 tile
    ds_ec = add_sentinel_tile_in_dataframe(ds_ec)

    ds_ec = ds_ec[['SiteName', 'Name', 'Country', 'CO',
                   'Longitude', 'Latitude', 'SateliteTile']]  

    return ds_ec


def matrix2listmap(arr, shape=None, group=None): 
    arr = array(arr)  
    if len(arr.shape) == 2: arr = array([arr])
    if group:
        shape = matrix(group[0]).shape
        if (arr.shape[0] == len(group)) & (arr.shape[1] != len(group)):
            arr = arr.T
        new_arr = [sum([ii[i][0] * matrix(group[i])
                    for i in range(len(group))])
                   for ii in array(arr)]
    else:
        shape = shape[-2:]  
        if (arr.shape[0] == shape[0] * shape[1]) & (arr.shape[1] != shape[0] * shape[1]):
            arr = arr.T
        new_arr = [[ii[i*(len(ii)//shape[0]):(i+1)*(len(ii)//shape[0])]
                    for i in range(shape[0])] 
                   for ii in array(arr)]
    
    new_arr = array(new_arr)
    assert(new_arr.shape[-2:] == shape)
    return new_arr


def listmap2matrix(arr):
    new_arr = [list(chain(*firstguess)) for firstguess in array(arr)]
    new_arr = array(new_arr)
    return new_arr


def matrix_listmap(arr, shape, direction=1):
    if direction==1: 
        return matrix2listmap(arr, shape)   
    if direction==-1:
        return listmap2matrix(arr)


def legend_without_duplicate_labels(ax):
    handles, labels = ax.get_legend_handles_labels()
    unique = [(h, l) for i, (h, l) in enumerate(zip(handles, labels)) if l not in labels[:i]]
    ax.legend(*zip(*unique))


def truncate_colormap(cmap, minval=0.0, maxval=1.0, n=100):
    new_cmap = cm.colors.LinearSegmentedColormap.from_list(
        'trunc({n},{a:.2f},{b:.2f})'.format(n=cmap.name, a=minval, b=maxval),
        cmap(linspace(minval, maxval, n)))
    return new_cmap


def Obukhov_Length(Ustar, Tv, H):
    k = 0.4 # von Karman
    g = 9.8 # [m/s2] gravitational acceleration
    # FHsfc = H
    FHsfc = -0.05 # kinematic surface heat flux, FHsfc, in units of degrees Kelvin meter per second (K m/s) 
    L = ((-Ustar**3)*Tv) / (k*g*FHsfc)
    return L


def plot_online(center, minZoom=6, FP=None, size=(700, 300), **kwargs):
    """
    Plot online
    Available with footprint
    Need to convert to EPSG:4326
    """
    if FP:
        FPcontour = {"rs": [None]*len(FP["rs"]),
                     "geometry": [None]*len(FP["rs"])}
        FPcontour["rs"] = FP["rs"]
        FPcontour["geometry"] = [gPolygon([[FP["xr"][i][j]] + [FP["yr"][i][j]] for j in range(len(FP["xr"][i]))])
                                 if FP["xr"][i] else None
                                 for i in range(len(FP["xr"]))]

        FPcontour = GeoDataFrame(pd.DataFrame(FPcontour), crs=3035)
        FPcontour = FPcontour.to_crs("EPSG:4326")  # reproject to 4326
        # drop empty geometries
        FPcontour = FPcontour[~FPcontour.geometry.isna()]

    ### site loc
    center = GeoDataFrame({"ds": ["site_loc"], "geometry": [
                              gPoint(center)]}, crs=3035)
    center = center.to_crs("EPSG:4326")
    center = center.geometry.values[0].coords[0]

    ## Create the map
    icos_map = fFigure(width=size[0], height=size[1])
    icos_map = fMap(location=[center[1], center[0]],
                          #zoom_start = zoom,
                          minZoom=minZoom,
                          maxBounds=[[center[1]-0.25, center[0]-0.25],
                                     [center[1]+0.25, center[0]+0.25]],
                          scrollWheelZoom=False,
                          control_scale=True,
                          **kwargs).add_to(icos_map)
    TileLayer(tiles='https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
                     attr='Esri', name='Esri Satellite', overlay=False, control=True).add_to(icos_map)

    if FP:
        groupr = FeatureGroup(name='R values')
        vcolors = [cm.get_cmap('rainbow', len(FPcontour["geometry"]))(
            i) for i in range(len(FPcontour["geometry"])+5)][::-1]

        FPcontour = FPcontour.sort_values(by='rs', ascending=False)
        for _, r in FPcontour.iterrows():
            #print(list(r.geometry.exterior.coords))
            Polygon(locations=[[el[1], el[0]] for el in list(r.geometry.exterior.coords)],
                           color=to_hex(vcolors[_]),
                           opacity=0.5,
                           weight=1,
                           fill_color=to_hex(vcolors[_]),
                           fill_opacity=0.3,
                           tooltip=r["rs"]).add_to(groupr)
        groupr.add_to(icos_map)

    # ICOS mast
    site_mast = BeautifyIcon(
        icon='star',
        inner_icon_style='color:red;font-size:20px;',
        background_color='transparent',
        border_color='transparent',
    )

    fMarker([center[1], center[0]], tooltip='SiteName',
                  icon=site_mast).add_to(icos_map)

    fmap.LayerControl('topright', collapsed=True).add_to(icos_map)
    fFullscreen(
        position='topright', force_separate_button=True).add_to(icos_map)
    return(icos_map)

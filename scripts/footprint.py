"""
Get Footprint Density Matrix
- Kljun et al. 2015
"""
import multiprocess as mp
import pickle
import scripts.common as tt
from scripts.FFP_Python.calc_footprint_FFP_climatology import FFP_climatology

class footprint:
    def __init__(self, df, loc=(0,0), **kwargs):#, TIMESTAMP, zm, z0, h, ol, sigmav, ustar, wd, **kwargs):
        "footprint"   
        self.meta = kwargs
        self.loc = loc

        needed_cols = ['TIMESTAMP', 'zm', 'z0',
                       'h', 'ol', 'sigmav', 'ustar', 'wd']
        for col in list(set(needed_cols) - set(df.columns)):
            df[col] = kwargs.pop(col, None)

        self.data = df[needed_cols]
        
    def get(self, filter=False, **kwargs):
        #FFPd = {}
        self.meta.update(kwargs)
        if not filter:
            filter = [True]*len(self.data)

        FFPd = FFP_climatology(zm=self.data.zm[filter].tolist(),
                               z0=self.data.z0[filter].tolist(),
                               h=self.data.h[filter].tolist(),
                               ol=self.data.ol[filter].tolist(),
                               sigmav=self.data.sigmav[filter].tolist(),
                               ustar=self.data.ustar[filter].tolist(),
                               wind_dir=self.data.wd[filter].tolist(),
                               **self.meta)
        FFPd = center_footprint(FFPd, centre=self.loc)
        self.FP = FFPd
        return self

    def geti(self, multiprocess=True, memory=True, **kwargs):
        FFPd = []
        self.meta.update(kwargs)
        meta_i = {k: v for k, v in self.meta.items() if k in [
            'dx', 'dy', 'nx', 'ny', 'rs']}

        if multiprocess:
            """Run assynchronously with a tip to reorder after"""
            print('Using MultiProcessing and ignoring Memory option.') if memory else None
            callback_dict = {}
            pool = mp.Pool(mp.cpu_count()-1)

        for k, t_ in enumerate(self.data.TIMESTAMP):
            input_i = {'zm': self.data.zm.tolist()[k],
                       'z0': self.data.z0.tolist()[k],
                       'h': self.data.h.tolist()[k],
                       'ol': self.data.ol.tolist()[k],
                       'sigmav': self.data.sigmav.tolist()[k],
                       'ustar': self.data.ustar.tolist()[k],
                       'wind_dir': self.data.wd.tolist()[k]}

            if multiprocess:
                pool.apply_async(__geti_mp__,
                                 args=(input_i, t_, meta_i),
                                 callback=lambda x: callback_dict.update(x))

            else:
                if memory:
                    filename = 'data/tmp/footprint/' + \
                        self.meta['name'] + '_' + t_.strftime('%Y%m%d%H%M')

                    meta_i = {k: v for k, v in self.meta.items(
                    ) if k in ['dx', 'dy', 'nx', 'ny', 'loc', 'rs']}
                    meta_i.update(input_i)
                    meta_i = tt.metadata(filename + '.meta', **meta_i)

                if memory and meta_i.check():
                    FFPd_i = pickle.load(open(filename + '.footprint', 'rb'))

                else:
                    FFPd_i = FFP_climatology(**input_i, **self.meta)
                    if memory:
                        pickle.dump(FFPd_i, open(
                            filename + '.footprint', 'wb'))
                        meta_i.write()

                FFPd = FFPd + \
                    [center_footprint(FFPd_i, centre=self.loc)]

        if multiprocess:
            pool.close()
            pool.join()

            print('Put back into the good order.')
            FFPd = sorted(callback_dict.items())
            order = [el[0] for el in FFPd]
            FFPd = [center_footprint(el[1], centre=self.loc) for el in FFPd]

            
        self.FP = FFPd
        self.order = order
        return self

    def plot(self, **kwargs):
        return tt.plot_online(center=self.loc, FP=self.FP, **kwargs)


def __geti_mp__(input, k, meta):
    from scripts.FFP_Python.calc_footprint_FFP_climatology import FFP_climatology
    return {k: FFP_climatology(**input, **meta)}

def center_footprint(FFPd, centre):
    ## meters to coordinates
    cx = lambda x : list(map(cx,x)) if isinstance(x, list) else x + centre[0] if x is not None else None
    cy = lambda x : list(map(cy,x)) if isinstance(x, list) else x + centre[1] if x is not None else None
    FFPd["xr"] = list(map(cx, FFPd["xr"]))
    FFPd["x_2d"] = list(map(cx, FFPd["x_2d"]))
    FFPd["yr"] = list(map(cy, FFPd["yr"]))
    FFPd["y_2d"] = list(map(cy, FFPd["y_2d"]))
    return(FFPd)

import scripts.common as tcom
import pandas as pd
from rasterio.io import MemoryFile
from rasterio import mask as msk, plot as rastplot, open as rastopen, band
from rasterio.warp import reproject
from shapely import geometry, ops
from pyproj import Transformer
import os
import re
import zipfile
from numpy import isnan, nan, sin, cos, radians, zeros

from matplotlib import cm, pyplot, lines as mplLine
from matplotlib.colors import rgb2hex

from fiona.drvsupport import supported_drivers
supported_drivers['KML'] = 'rw'


#def download_satdata(stdate='2021-03-02', endate='2021-03-31', tile='T31UDQ', sat='SENTINEL2', cloudcover='50'):
def download_satdata(stdate, endate, tile, sat='SENTINEL2', cloudcover='50'):
    # function to run theia_download.py
    # you should have configured config_theia.cfg
    import subprocess

    with subprocess.Popen(['cmd',
                             '/k python ./theia_download.py -t ' + tile + ' -c ' + sat + 
                             ' -a config_theia.cfg -d ' + stdate + ' -f ' + endate + ' -m' + str(cloudcover)],
                          stdout=subprocess.PIPE, cwd="data/sat/SENTINEL2", 
                          bufsize=1, universal_newlines=True) as p:
        for line in p.stdout:
            print(line, end='')
    
    return()


def distfromtower(center, countour, wd, verbosity=0):
    if isnan(wd):
        return nan
    ln = geometry.LineString(
        [(center), (center[0] + 500 * sin(radians(wd)), 
                         center[1] + 500 * cos(radians(wd)))])
    intersec = countour.intersection(ln)
    if verbosity:
        pyplot.plot(*ln.xy)
        pyplot.plot(*countour.xy)
        pyplot.scatter(*intersec.xy)
    return geometry.Point(center).distance(intersec)


def crop_tif_by_polygon(cutshape, basegrid, extent):
    # time consuming !
    # crop meshgrid by checking each point if it is inside a polygon 
    outputgrid = zeros(basegrid.shape)  #.astype(float) 
    outputgrid[:] = nan
    
    for i in range(len(basegrid)):
        for j in range(len(basegrid[i])):
            lon = min(extent[0:2]) + j * abs(extent[1] - extent[0]) / len(basegrid[i])
            lat = min(extent[2:4]) + i * abs(extent[3] - extent[2]) / len(basegrid)
            #if (cutshape and cutshape.contains(geometry.Point(lon, lat))) or not cutshape:
            if cutshape.contains(geometry.Point(lon, lat)):
                outputgrid[i][j] = basegrid[i][j]
    return outputgrid


def cut_proj(infile, center, buffer, extentBox=None, outfolder="data/tmp/", outfile=None, force=False, plot=False, verbosity=0, **kwargs):
    
    if not os.path.exists(outfolder):
        os.makedirs(outfolder)
        
    if outfile is None:
        cut_tif = outfolder + "cut_" + os.path.basename(infile) # "data/temp/cut_OCS_2020.tif"
        outfile = os.path.splitext(cut_tif)[0] + "_reproj.tif" # "data/temp/cut_OCS_2020_reproj.tif"
    out_metadata = os.path.splitext(outfile)[0] + ".metadata"
    
    cutproj_meta = tcom.metadata(out_metadata, center=center, offset=buffer)
    same_metadata = os.path.exists(outfile) and\
        False #cutproj_meta.check()
        
    if same_metadata==False or force:
        if verbosity>0:
            print("Trimming tif file.")
        
        if extentBox and len(extentBox)==4:
            gbox = geometry.Polygon([[extentBox[0], extentBox[2]], 
                                     [extentBox[1], extentBox[2]], 
                                     [extentBox[1], extentBox[3]], 
                                     [extentBox[0], extentBox[3]]])
        elif extentBox:
            gbox = extentBox
        else:
            gbox = geometry.Polygon([[center[0]-buffer, center[1]-buffer],
                                     [center[0]+buffer, center[1]-buffer],
                                     [center[0]+buffer, center[1]+buffer],
                                     [center[0]-buffer, center[1]+buffer]])

        with rastopen(infile) as src:
            # reproject slighlty bigger box
            transformer = Transformer.from_crs(
                "EPSG:3035", src.crs, always_xy=True).transform
            gbox_rep = ops.transform(transformer, gbox.buffer(100))
           
            # larger cut, for faster reprojection
            out_cut_image, out_cut_trans = msk.mask(src, [gbox_rep], crop=True)

            # translate back to tif format
            src_cut = MemoryFile().open(driver='GTiff', height=out_cut_image.shape[1], width=out_cut_image.shape[2], count=1, crs=src.crs,
                                        transform=out_cut_trans, dtype=out_cut_image.dtype)
            src_cut.write(out_cut_image)
            
            # reproject to 3035
            src_reproj, src_reproj_trans = reproject(
                source=band(src_cut, 1), dst_crs='EPSG:3035')
            #src_reproj = src_reproj[0]
            
            # translate back to tif format
            src_rep = MemoryFile().open(driver='GTiff', height=src_reproj.shape[1], width=src_reproj.shape[2], count=1, crs='EPSG:3035',
                                        transform=src_reproj_trans, dtype=src_reproj.dtype)
            src_rep.write(src_reproj)
            
            # precision cut
            out_image, out_transform = msk.mask(src_rep, [gbox], crop=True)
            out_meta = src.meta.copy()
            out_meta.update({"driver": "GTiff",
                            "height": out_image.shape[1],
                            "width": out_image.shape[2],
                            "transform": out_transform,
                            "crs": 'EPSG:3035'})
            # , "crs": pycrs.parser.from_epsg_code(epsg_code).to_proj4()})
            
        """
        with rastopen(infile) as src:
            transformer = Transformer.from_crs("EPSG:3035", src.crs, always_xy=True).transform
            gbox = ops.transform(transformer, gbox)
            # print(gbox.bounds)
            # print(src.bounds)
            out_image, out_transform = msk.mask(src, [gbox], crop=True)
            out_meta = src.meta.copy()
            #print(out_meta)
        """
                
        with rastopen(outfile, "w", **out_meta) as dest:
            dest.write(out_image)
        
        cutproj_meta.write()
    
    clipped = rastopen(outfile)
    if plot:
        rastplot.show((clipped, 1), cmap='terrain')
        
    return clipped

def get_nearest_satelite(satfolder, tile):
    """Find dates we already have the file"""
    theia_folder_pattern = "^SENTINEL2[AB]?_(\d{8})-\d{6}-\d{3}_L2A_[T]?" + \
        tile+"_D(.zip)?$"
    theia_dowloaded_dates = [re.match(theia_folder_pattern, folder).group(1) for folder in os.listdir(satfolder)
                             if re.match(theia_folder_pattern, folder) is not None]

    if os.path.isdir(satfolder + "JP2/" + tile + "/"):
        s2_dowloaded_dates = [re.match("^(\d{8})$", folder).group(1) for folder in os.listdir(satfolder + "JP2/" + tile + "/")
                                  if re.match("^(\d{8})$", folder) is not None and len(os.listdir(satfolder + "JP2/" + tile + "/" + folder)) > 0]
    else:
        s2_dowloaded_dates = []

    if len(theia_dowloaded_dates) > 0:
        source = 'theia'
        dowloaded_dates = theia_dowloaded_dates

    elif len(s2_dowloaded_dates) > 0:
        source = 'copernicus'
        dowloaded_dates = s2_dowloaded_dates

    return source, dowloaded_dates
            

def get_path_to_satelite(date,
                         satfolder='data/sat/SENTINEL2/',
                         tile='T31UDQ', download=False, verbosity=1, 
                         maxiter=16, **kwargs):

    acceptable_range_dates = 0
    source = ''
        
    while True:
        """While used to only download the nearest date"""
        acceptable_range_dates += 1

        if abs(acceptable_range_dates) > maxiter:
            print("No satelite data found whithin" + str(maxiter) + "days from the observation date.")
            print('Date not available, you should download it, follow steps below.', '')
            with open(satfolder + 'theia_python.txt', 'r') as helpfile:
                print(helpfile.read())
            return(None, None, None)

        source, dowloaded_dates = get_nearest_satelite(satfolder, tile)

        sat_date, sat_date_dif = tcom.nearest(
            [pd.to_datetime(d) for d in set(dowloaded_dates)], date)
        if sat_date_dif.days <= acceptable_range_dates:
            break

        if download:
            print("Date (+- ",  acceptable_range_dates,
                  " days) not available, downloading data...\n", "It may take a while (hours).")
            download_satdata(stdate=str((date - pd.DateOffset(acceptable_range_dates)).date()),
                             endate=str(
                                 (date + pd.DateOffset(acceptable_range_dates)).date()),
                             tile=tile, **kwargs)


    # From this point on we already have the sat file downloaded (zipped or unzipped)
    if verbosity > 0:
        print("Satelite data found is closest to the observation date by " +
              str(sat_date_dif.days) + " days.")

    sat_date = str(sat_date.date()).replace('-', '')

    if source == 'theia':
        folder_pattern = "^(SENTINEL2[AB]?_" + \
            sat_date + "-\d{6}-\d{3}_L2A_[T]?"+tile+"_D(.zip)?)$"
        sat_foldername = [satfolder + re.match(folder_pattern, folder).group(1) for folder in os.listdir(satfolder)
                          if re.match(folder_pattern, folder) is not None]

    if source == 'copernicus':
        sat_foldername = [satfolder + "/JP2/" + tile + "/" + sat_date + "/"]

    if len([f for f in sat_foldername if os.path.isdir(f)]) > 1 or \
            len([f for f in sat_foldername if f.endswith('.zip')]) > 1:
        print("Warning: more than 1 folder found, using only first folder.")
    
    if [f for f in sat_foldername if os.path.isdir(f)]:
        sat_foldername = [f for f in sat_foldername if os.path.isdir(f)][0]
    else:
        sat_foldername = os.path.splitext(sat_foldername[0])[0]

    return(sat_foldername, sat_date, sat_date_dif)

    
def get_sat_data(sat_date, center,
                 sat_foldername, sat_date_dif,
                 sat_tempfolder = 'data/tmp/SENTINEL2/', 
                 ref='SRE', bands=None, extentBox=None, buffer=0.05, plot=False, force=False,
                 verbosity=1, **kwargs): 
    # get satellite data, whether downloading it, unzipping or simply oppening it
    # For the codes ...MG2_B1.tif, check:
    # https://labo.obs-mip.fr/multitemp/landsat/theias-landsat-8-l2a-product-format/
    # https://gisgeography.com/sentinel-2-bands-combinations/
    
    #sat_foldername = '../eddy-tower_largefiles/data/SENTINEL2/SENTINEL2B_' + sat_date + '-105727-281_L2A_T31UDQ_D'
    sat_tempfolder =  sat_tempfolder + sat_date + '/'
        
    # if folder doesn't exist but zip exists, unzip
    if os.path.isdir(sat_foldername)==False and os.path.exists(sat_foldername + '.zip'):
        if verbosity>0:
            print("Unzipping...")
        with zipfile.ZipFile(sat_foldername + '.zip', 'r') as zip_ref:
            zip_ref.extractall(sat_foldername)
                
    sat_data = []

    for root, directories, files in os.walk(sat_foldername):
        for tif in files:
            if bool(re.search(ref + "_B\d.?\.(tif)$", tif)):
                if ((bands == None) or (re.search("_(B\d.?)\.(tif)$", tif).group(1) in bands)):
                    sat_data = sat_data + [cut_proj(os.path.join(root, tif), center=center, 
                                                    outfolder=sat_tempfolder, extentBox=extentBox,
                                                    buffer=buffer, force=force, plot=False)]
            if bool(re.search("_B0?\d.?\.jp2$", tif)):
                if ((bands == None) or ('B' + re.search("_B(\d.+)\.jp2$", tif).group(1).lstrip('0') in bands)):
                    sat_data = sat_data + [cut_proj(os.path.join(root, tif), center=center,
                                                    outfolder=sat_tempfolder, extentBox=extentBox,
                                                    buffer=buffer, force=force, plot=False)]

    if plot:
        for i in range(min(len(sat_data), 10)):
            extent = rastplot.plotting_extent(sat_data[i])[:2] + rastplot.plotting_extent(sat_data[i])[3:] + rastplot.plotting_extent(sat_data[i])[2:3]
            fig = pyplot.figure()
            pyplot.contourf(sat_data[i].read(1), cmap='terrain_r', extent=extent, zorder=0)
    
    satdata_meta = tcom.metadata()
    satdata_meta.bands = {'B' + re.match(".*_B(..?)_reproj.tif$", sat_data[i].name).group(
        1).lstrip('0'): i for i in range(len(sat_data))}
    satdata_meta.datedif = sat_date_dif
    
    return(sat_data, satdata_meta)


def plot_sat(*args, title=None, xlabel=None, ylabel=None,
             footprint=None, pcfp=None, mast=None, cb=True, 
             scales='fix', origin='lower',
             levels=None, ax=None, **kwargs):
    #fig = pyplot.figure()
    ax = pyplot.axes() if ax is None else ax
    ax.set_title(title)
    if len(args) == 1:
        X, Y, contourf = None, None, args[0]
    elif len(args) == 3:
        X, Y, contourf = args
    else:
        print('Shape', len(args), 'not allowed. Use X, Y, Z or only Z.')
    if len(contourf.shape) == 3:
        """typically RGB(A) data"""
        contourf = contourf.transpose([1, 2, 0])
        #pyplot.imshow(contourf.transpose([1, 2, 0]), zorder=0, **kwargs)
    if scales=="free" and len(contourf.shape) == 2:
        if X is not None and Y is not None:
            colfig = ax.contourf(
                X, Y, contourf, levels=levels, zorder=0, origin=origin, **kwargs)
        else:
            colfig = ax.contourf(contourf, levels=levels,
                                 origin=origin, zorder=0, **kwargs)
    else:
        colfig = ax.imshow(contourf, zorder=0, origin=origin, **kwargs)
    
    if footprint:
        unique_rs = sorted(list(set(footprint["rs"])))
        colors_rs = cm.get_cmap('bwr', len(unique_rs))
        colors = {unique_rs[i]: rgb2hex(colors_rs(i)) for i in range(len(unique_rs))}
        for n in range(len(footprint["xr"])): 
            if pcfp:
                if footprint["rs"][n]==pcfp and footprint["xr"][n] is not None:
                    ax.plot(footprint["xr"][n], footprint["yr"][n], label=str(footprint["rs"][n]), color='k', zorder=9)
            else:
                if footprint["xr"][n] is not None:
                    ax.plot(footprint["xr"][n], footprint["yr"][n], label=str(footprint["rs"][n]), color=colors[footprint["rs"][n]], zorder=9)
    
        lines_rs = [mplLine.Line2D([0], [0], color=colors[el], lw=4) for el in unique_rs]
        ax.legend(lines_rs, unique_rs)
    #tcom.legend_without_duplicate_labels(pyplot.axes())
    
    if cb: pyplot.colorbar(colfig, ax=ax)
    
    if mast:
        ax.plot(mast[0], mast[1], marker='*', color='red', markersize=10)
        # pyplot.plot(3761997.03, 2889405.83, marker='*', color='blue', markersize=10)  # Paris center
    
    ax.set_xlabel("East (ETRS89)") if xlabel is None else ax.set_xlabel(xlabel)
    ax.set_ylabel("North (ETRS89)") if ylabel is None else ax.set_ylabel(ylabel)
    
    #if keepFig:
    #    return(keepFig)
    #pyplot.show()
    #pyplot.close()
    return

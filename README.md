# Interactive ICOS Satelite Maps v. alpha

## Getting started

> cd PATH_TO_GIT_FOLDER

A. open Anaconda Prompt<br>
> conda create -n interface_sat_for_icos python=3.8<br>
> conda activate interface_sat_for_icos

B. open cmd<br>
> pip install virtualenv<br>
> virtualenv interface_sat_for_icos<br>
> source interface_sat_for_icos/bin/activate

> pip install -r requirements.txt<br>
> python run.py

## Downloading satelite data

> Go to [Theia](https://www.theia-land.fr/) or [Copernicus](https://scihub.copernicus.eu/dhus/#/home).<br>
> Save zip file on 'PATH_TO_GIT_FOLDER/data/sat/SENTINEL2/'

:: Open script run.py

:: call %HOMEPATH%\Anaconda3\Scripts\activate.bat %HOMEPATH%\Anaconda3

set "venv=iicossataddon"

@echo "Working on environment %venv%"

:Begin

call conda activate %venv%

if %ERRORLEVEL% neq 0 goto CondaFromPath

:CondaFromPath

call %HOMEPATH%\Anaconda3\Scripts\activate.bat %HOMEPATH%\Anaconda3

call activate %venv%

if %ERRORLEVEL% neq 0 goto VenvError

cd %~dp0

python run.py

if %ERRORLEVEL% neq 0 goto ReqError

exit /b 0

:VenvError
@echo "Creating environment %venv%"

call conda create -n "%venv%" python=3.8

goto ReqError

exit /b 1

:ReqError
@echo "Getting requirements."

call conda activate %venv%

call pip install -r requirements.txt

goto Begin

exit /b 1

:: pause

:: call %~dp0run.bat
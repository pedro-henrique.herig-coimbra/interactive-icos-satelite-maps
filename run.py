import webbrowser
import scripts.common as tt
import scripts.satelite_products as satprod
import scripts.satelite as satelite
from pandas import to_datetime, DataFrame, read_excel, unique
from numpy import nan, abs as npabs
from numpy.random import random as nprand, normal as npnorm
from numpy.ma import array as maarray

import tkinter as tk
import tkinter.ttk as ttk
import scripts.tk_talltower as tktt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
#from tkinter import Tk, Canvas, PhotoImage, mainloop, Button, YES, NO, BOTH, NW, Label, filedialog
import PIL
#from PIL import Image, ImageDraw, ImageTk
import matplotlib.pyplot as plt


def save():
    # filename = f'data/temp/drawing_{time.now()}.png'
    filename = tk.filedialog.asksaveasfilename(initialdir="data/pub/",
                                          title="Select a File",
                                          filetypes=((".png",
                                                      "*.png*"),
                                                     ("all files",
                                                      "*.*")),
                                            defaultextension=((".png",
                                                      "*.png*"),
                                                     ("all files",
                                                      "*.*")))

    #image1.save(filename)
    figure1.savefig(filename)


def allow_paint():
    import re
    commands = cv.__dict__['_tclCommands']
    if re.search('activate_paint', ''.join(commands)):
        cv.unbind('<B1-Motion>')
        cv.unbind('<1>')
    else:
        cv.bind('<B1-Motion>', paint)
        cv.bind('<1>', activate_paint)


def activate_paint(e):
    global lastx, lasty
    cv.bind('<B1-Motion>', paint)
    lastx, lasty = e.x, e.y


def paint(e):
    global lastx, lasty
    x, y = e.x, e.y
    cv.create_line((lastx, lasty, x, y), width=1)
    #  --- PIL
    draw.line((lastx, lasty, x, y), fill='black', width=1)
    lastx, lasty = x, y


def cls():
    cv.delete('all')


def clear(frame):
    for widget in frame.winfo_children():
        widget.destroy()

def new_draw():
    global draw
    # --- PIL
    image1 = PIL.Image.new('RGB', (640, 480), 'white')
    draw = PIL.ImageDraw.Draw(image1)


def viewSelected():
    global currentPreview
    currentPreview = previewFrame
    #reset_frame(display3)
    #if rmap2plot.get() != 'online':
    #    new_window()
    plot_map()

def viewVPRM():
    global currentPreview
    global rmap2plot
    currentPreview = previewFrame
    #reset_frame(display3)
    #if rmap2plot.get() != 'online':
    #    new_window()
    rmap2plot.set('vprm')
    plot_map()

def newpageSelected():
    new_window()
    plot_map(nwind, menu_options=['save'])


def new_window():
    global nwind
    global currentPreview
    nwind = tk.Toplevel(display1)
    currentPreview = nwind


def menu_window(widget=None, menu_buttons=[]):
    global previewFrame

    menu = tk.Frame(widget, width=0, height=0)
    menu.pack()

    if not menu_buttons or 'refresh' in menu_buttons:
        tk.Button(menu, text="refresh", command=plot_map).grid(column=0, row=0)
    if not menu_buttons or 'nwin' in menu_buttons:
        tk.Button(menu, text="new window", command=newpageSelected).grid(column=1, row=0)
    if not menu_buttons or 'draw' in menu_buttons:
        tk.Button(menu, text="draw", command=allow_paint, state='disabled').grid(column=2, row=0)
    if not menu_buttons or 'clear' in menu_buttons:
        tk.Button(menu, text="clear", command=cls, state='disabled').grid(column=3, row=0)
    if not menu_buttons or 'save' in menu_buttons:
        tk.Button(menu, text="save", command=save).grid(column=4, row=0)


def plot_map(widget=None, menu_options=[]):
    global cv
    global datetime2plot
    global previewFrame
    global currentPreview
    global figure1

    if widget is None:
        widget = currentPreview

    datetime2plot = datetime2plot if datetime2plot else to_datetime('20210301')
    map2plot = 'LAND USE' if not rmap2plot.get() else rmap2plot.get().upper()

    site_ec = tt.ECsite(listSitelb.get(tk.ANCHOR))

    maptitle = map2plot + ' (' + site_ec.name + ')\n' + str(datetime2plot.date())
    maptitle = maptitle + ', ' + ', '.join(luscr) if luscr else maptitle

    if map2plot == 'ONLINE':
        import os
        import webbrowser

        flmap = tt.plot_online(site_ec.loc, startZoom=600,
                            minZoom=1, size=(640, 160))
        flmap.save(wrkpath.get("1.0", "end-1c")+'/tmp/foliummap.html')
        webbrowser.open(os.path.realpath(
            wrkpath.get("1.0", "end-1c")+'/tmp/foliummap.html'))
        return

    figure1 = plt.Figure(figsize=(10, 5), dpi=100)
    ax = figure1.add_subplot(111)

    kwargs = {}
    if map2plot == "LAND USE":
        kwargs.update({'cmap': 'terrain_r'})
    if map2plot == "RGB":
        kwargs.update({'vmin': 0, 'vmax': 255, 'cb': False})

    if map2plot == "VPRM":
        vprm_needs = {'PAR': [VPRMinput_PAR.get()], 'T': [VPRMinput_TA.get()], 
                      'PAR0': VPRMinput_PAR0.get(), 'Tlow': VPRMinput_Tlow.get(),
                      'Tmax': VPRMinput_Tmax.get(), 'Tmin': VPRMinput_Tmin.get(), 'Topt': VPRMinput_Topt.get(), 
                      'α': VPRMinput_α.get(), 'β': VPRMinput_β.get(), 'λ': VPRMinput_λ.get()}
    else:
        vprm_needs = {}


    bgmap = satprod.satprod().get(map2plot, center=site_ec.loc, buffer=max(bufferMap.get(), 200),
                                  timelist=datetime2plot, tile=site_ec.tile, download=False, filepath=site_ec.lu_path, 
                                  verbosity=0, **vprm_needs, 
                                  satfolder=satpath.get("1.0", "end-1c") + '/SENTINEL2/')

    if FPactivate.get()==1:
        from scripts.footprint import footprint
        FP = footprint(loc=site_ec.loc,
                       df=DataFrame({'zm': [FPinput_Zm.get()]*FPinput_N.get(),
                                        'z0': [FPinput_Zm.get()/1000]*FPinput_N.get(),
                                        'h': [1000]*FPinput_N.get(),
                                        'ol': [FPinput_OL.get()]*FPinput_N.get(),
                                        'sigmav': [FPinput_SV.get()]*FPinput_N.get(),
                                        'ustar': [FPinput_US.get()] if FPinput_N.get() == 1 else list(npabs(npnorm(FPinput_US.get(), 0.1, FPinput_N.get()))),
                                        'wd': [FPinput_WD.get()] if FPinput_N.get() == 1 else list(nprand(FPinput_N.get())*360)
                                        }))
        FP.get(r=[10,20,30,40,50,60,70,80,90], dx=bgmap.dx, dy=bgmap.dy,
               nx=bgmap.data.shape[-1]-1, ny=bgmap.data.shape[-2]-1, verbosity=0)
        FP = FP.FP
    else:
        FP = None

    if luscr and map2plot not in ["LAND USE", "RGB"]:
        for lu_ in luscr:
            if site_ec.lu_resolution == 20:
                d = d*2
            lu_data = satprod.satprod().get('LU', center=site_ec.loc, buffer=max(bufferMap.get(), 200),
                                            filepath=site_ec.lu_path, **kwargs).data
            
            lu_meta = read_excel(site_ec.lu_metapath).set_index(
                'code')[['name', 'IGBP']].to_dict()
            lu_dic = {}
            for k, v in lu_meta['IGBP'].items():  # <- missing ()
                lu_dic[v] = lu_dic.get(v, [])
                lu_dic[v].append(k)
            
            lu_data = satprod.force_array_dimension(bgmap.data[0].shape, lu_data)
            lu_data = sum([maarray(lu_data, mask=lu_data == el)
                           for el in lu_dic[lu_]])
            lu_data[~lu_data.mask] = nan
            lu_data[lu_data.mask] = 1
            lu_filter = tt.sum_nan_arrays(lu_filter, lu_data) if 'lu_filter' in locals() else lu_data

        bgmap.plot(scales="free", ax=ax, title=map2plot + ' (' + site_ec.name + \
                   ')\n' + str(datetime2plot.date()), cmap='Greys', ** kwargs)
        bgmap.data = bgmap.data * lu_filter
        bgmap.plot(scales="free", ax=ax, title=maptitle, footprint=FP, ** kwargs)
    else:
        bgmap.plot(scales="free", ax=ax, title=maptitle,
                   footprint=FP, ** kwargs)


    scalex = min(bgmap.extent[:2]) + 0.9 * (max(bgmap.extent[:2])-min(bgmap.extent[:2]))
    scaley = min(bgmap.extent[2:4]) + 0.1 * \
        (max(bgmap.extent[2:4])-min(bgmap.extent[2:4]))
    ax.plot([scalex-plotScale.get(), scalex], [scaley, scaley], color='k')
    ax.plot([scalex, scalex], [scaley, scaley+plotScale.get()], color='k')

    clear(widget)
    cv = FigureCanvasTkAgg(figure1, widget).get_tk_widget()
    cv.pack(
        expand=tk.YES, fill=tk.BOTH)
    menu_window(widget, menu_options)
    #new_draw()

    plt.close()


def luChoice(lu):
    global luscr
    if isinstance(lu, list):
        [luChoice(lu_) for lu_ in luscr]
        [luChoice(lu_) for lu_ in lu]
        return

    if lu in luscr:
        luscr = [lu_ for lu_ in luscr if lu_ != lu]
    else:
        luscr = luscr + [lu]

    for lu in ['URB', 'CRO', 'GRA', 'DF', 'EF']:
        if lu not in luscr:
            dLUimg[lu]['tkBt'].configure(
                image=dLUimg[lu]['tkBt'].image_off)
        else:
            dLUimg[lu]['tkBt'].configure(image=dLUimg[lu]['tkBt'].image)

    luLabel.configure(text=', '.join(
        ['\n' + str(el) if i > 1 and i % 2 == 0 else el for i, el in enumerate(luscr)]))

def dateChoice(e=None):
    global datetime2plot
    try: 
        datetime2plot = to_datetime(listDatelb.get(tk.ANCHOR))
        #btdate2plot.configure(text='-> ' + str(datetime2plot))
    except ValueError:
        datetime2plot = None
        #btdate2plot.configure(text='-> ValueError')

def updateDateList(e=None):
    global datetime2plot
    datetime2plot = None
    dateChosen.configure(text='')
    listDatelb.delete(0, tk.END)
    try:
        [listDatelb.insert(i, el) for i, el in enumerate(sorted(set(satelite.get_nearest_satelite(
            satpath.get("1.0", "end-1c") + '/SENTINEL2/', tile=tt.ECsite(listSitelb.get(tk.ANCHOR)).tile)[1])))]
    except:
        return

def countryChoice(e=None):
    site_selection = read_excel(wrkpath.get("1.0", "end-1c")+"/stations.xlsx")
    if countryChosen.cget("text"):
        site_selection = site_selection[site_selection.CO == countryChosen.cget("text")]
    site_selection = tt.prioritize_list(site_selection.Name.to_list(), "(Grig)|(Hegyhátsál)|(Sac)|(Ris)")
    return site_selection

def updateSiteList(e=None):
    siteChosen.configure(text='')
    listSitelb.delete(0, tk.END)
    try:
        [listSitelb.insert(i, el) for i, el in enumerate(countryChoice())]
    except:
        return

"""
Globals
"""
datetime2plot = to_datetime('20210301')

root = tk.Tk()
root.grid_columnconfigure(1, weight=1)

#holderframe = tk.Frame(root)
#holderframe.grid(row=0, column=0, sticky=tk.N + tk.S + tk.W + tk.E)

shellmenu = tktt.VerticalScrolledFrame(root)
shellmenu.grid(row=0, column=0, sticky=tk.N + tk.S + tk.W + tk.E)
#shellmenu.pack(expand=1, fill=tk.BOTH)
holderframe = shellmenu.interior
holderframe.grid_columnconfigure(1, weight=1)

display1 = tk.Frame(holderframe)
display_wrk = tk.Frame(holderframe)
display2 = tk.Frame(holderframe)
display3 = tk.Frame(holderframe)
display5 = tk.Frame(holderframe)

previewframe = tk.Frame(root)
previewframe.grid(row=0, column=1, sticky=tk.N + tk.S + tk.W + tk.E)

"""
Header ___________
"""
##display_green
display1.pack(fill=tk.X)
#display1.grid(column=0, row=0)
#display1.pack_propagate(0)  # when using pack inside of the display
#display.grid_propagate(0) #when using grid inside of the display

#luIcon = PIL.Image.open(
#    'data/pub/static/land use icons/' + in_['file']).resize((50, 50))
headerImg = PIL.Image.open(
    'data/pub/static/header.png')
headerImg = headerImg.resize((int(50*headerImg.size[0]/headerImg.size[1]), 50))
headerImg = PIL.ImageTk.PhotoImage(headerImg, master=display1)
headerR = tk.Label(display1, image=headerImg)
headerR.pack()
headerR.image =  headerImg

frameAuthor = tk.Frame(display1)
frameAuthor.pack()

tk.Label(frameAuthor, text='Pedro H. H. Coimbra\n B. Loubet\n O. Laurent').grid(row=0, column=0)
headerIcon = PIL.Image.open(
    'data/pub/static/visual_abstract_these_2.png')
headerIcon = headerIcon.resize((int(50*headerIcon.size[0]/headerIcon.size[1]), 50))
headerIcon = PIL.ImageTk.PhotoImage(headerIcon, master=frameAuthor)
headerL = tk.Label(frameAuthor, image=headerIcon)
headerL.grid(row=0, column=1, sticky=tk.E)
headerL.image =  headerIcon

"""
Wrk Path ___________
"""
display_wrk.pack(fill=tk.X)
display_wrk.grid_columnconfigure(1, weight=1)

tk.Label(display_wrk, text='Data folder:').grid(row=0, column=0)

wrkpath = tk.Text(display_wrk, height=1, width=50)
wrkpath.insert(tk.END, 'data/')  # ../eddy-tower_largefiles/data/input/raw
wrkpath.grid(row=0, column=1, sticky=tk.W)

tk.Label(display_wrk, text='Sat. folder:').grid(row=1, column=0)

satpath = tk.Text(display_wrk, height=1, width=50)
satpath.insert(tk.END, 'data/sat/')  # ../eddy-tower_largefiles/data/input/raw
satpath.grid(row=1, column=1, sticky=tk.W)

"""
Main Menu ___________
"""
##display2_orange
display2.pack(expand=1, fill= tk.BOTH)
#display2.grid(column=0, row=1, sticky=tk.N + tk.S + tk.W + tk.E)
#display2.grid_propagate(0)
display2.grid_columnconfigure(1, weight=1)

"""
Choosen Site & Date
"""
frameLabelSiteDate = tk.Frame(display2)
frameLabelSiteDate.grid(row=1, column=1, sticky=tk.W + tk.E)

countryChosen = tk.Label(frameLabelSiteDate, text='')
countryChosen.grid(row=0, column=0)

siteChosen = tk.Label(frameLabelSiteDate, text='')
siteChosen.grid(row=0, column=1)

dateChosen = tk.Label(frameLabelSiteDate, text='')
dateChosen.grid(row=0, column=2)

"""
Choose Site & Date
"""

frameSiteDate = tk.Frame(display2)
frameSiteDate.grid(row=0, column=1, sticky=tk.W + tk.E)
frameSiteDate.grid_columnconfigure(0, weight=1)
frameSiteDate.grid_columnconfigure(2, weight=1)

listCountrysb = tk.Scrollbar(frameSiteDate)
listCountrysb.grid(row=0, column=1, sticky=tk.N+tk.S)

listCountrylb = tk.Listbox(frameSiteDate, width=3, height=5)
listCountrylb.grid(row=0, column=0, sticky=tk.W+tk.E)

listCountrylb.configure(yscrollcommand=listCountrysb.set)
listCountrysb.config(command=listCountrylb.yview)

[listCountrylb.insert(i, el) for i, el in enumerate([''] + tt.prioritize_list(list(unique(read_excel(
    wrkpath.get("1.0", "end-1c")+"/stations.xlsx").CO)), "(FR)|(DK)|(HU)"))]

listCountrylb.bind('<<ListboxSelect>>', lambda e: countryChosen.configure(
    text=listCountrylb.get(tk.ANCHOR)) if e.widget.curselection() else None)
countryChosen.configure(text=listCountrylb.get(tk.ANCHOR))

# Sites
listSitesb = tk.Scrollbar(frameSiteDate)
listSitesb.grid(row=0, column=3, sticky=tk.N+tk.S)

listSitelb = tk.Listbox(frameSiteDate, height=5)
listSitelb.grid(row=0, column=2, sticky=tk.W + tk.E)

listSitelb.configure(yscrollcommand=listSitesb.set)
listSitesb.config(command=listSitelb.yview)

#[listSitelb.insert(i, el) for i, el in enumerate(countryChoice())]

listCountrylb.bind('<<ListboxSelect>>', updateSiteList, '+')
listCountrylb.select_anchor(0)

#listSitelb.bind('<<ListboxSelect>>', lambda e: siteChosen.configure(text=e.widget.get(
#    e.widget.curselection())) if e.widget.curselection() else None, '+')
listSitelb.bind('<<ListboxSelect>>', lambda e: siteChosen.configure(text=e.widget.get(e.widget.curselection())) if e.widget.curselection() else None)
siteChosen.configure(text=listSitelb.get(tk.ANCHOR))

# Dates
listDatesb = tk.Scrollbar(frameSiteDate)
listDatesb.grid(row=0, column=5, sticky=tk.N + tk.S + tk.E)

listDatelb = tk.Listbox(frameSiteDate, height=5)
listDatelb.grid(row=0, column=4, sticky=tk.W + tk.E)

listDatelb.configure(yscrollcommand=listDatesb.set)
listDatesb.config(command=listDatelb.yview)

listSitelb.bind('<<ListboxSelect>>', updateDateList, '+')
listSitelb.select_anchor(0)

listDatelb.bind('<<ListboxSelect>>', dateChoice)
listDatelb.bind('<<ListboxSelect>>', lambda e: dateChosen.configure(text=to_datetime(e.widget.get(
    e.widget.curselection())).date()) if e.widget.curselection() else None, '+')


def winDownloadSatDate():
    new_window()
    nwind.title('VPRM Equations')
    
    tile = read_excel(
        wrkpath.get("1.0", "end-1c")+"/stations.xlsx")
    tk.Label(nwind, text=listSitelb.get(tk.ANCHOR), justify='left').pack()
    tile = tile[tile.Name == listSitelb.get(tk.ANCHOR)].SateliteTile.values[0]

    tk.Label(nwind, text='tile: ' + tile, justify='left').pack()
    tk.Label(nwind, text='start and end (format: YYYY-MM-DD YYYY-MM-DD)', justify='left').pack()

    datetk = tk.Text(nwind, height=2)
    datetk.insert(tk.END, '2021-03-01 2021-03-02')
    datetk.pack()

    #tk.Button(nwind, text='Show',
    #          command=lambda: datetk.insert(tk.END, datetk.get("1.0", "end-1c"))).pack()
    
    winDownloadbuttons = tk.Frame(nwind)
    winDownloadbuttons.pack()

    tk.Button(winDownloadbuttons, text='website',
              command=lambda: webbrowser.open('https://scihub.copernicus.eu/dhus/#/home')).grid(row=0, column=0)

    tk.Button(winDownloadbuttons, text='Download',
              command=lambda: satelite.download_satdata(stdate=datetk.get("1.0", "end-1c").split(' ')[0],
                                                        endate=datetk.get(
                                                            "1.0", "end-1c").split(' ')[1],
                                                        tile=tile),
              state='disabled').grid(row=0, column=1)


tk.Button(frameSiteDate, text='Download', command=winDownloadSatDate).grid(row=0, column=4, sticky=tk.E)

"""
Choose Map Buffer
"""
tk.Label(display2, text='Buffer (m): ').grid(row=3, column=0)
bufferMap = tk.Scale(display2, from_=0, to=5000, orient=tk.HORIZONTAL,
                     command=lambda x: bufferMap.set(500 * (int(x)//500)))
bufferMap.set(1000)
bufferMap.grid(row=3, column=1, sticky=tk.W + tk.E)

"""
Choose size for Map Scale Bar
"""
tk.Label(display2, text='Scale \n(to plot): ').grid(row=4, column=0)
plotScale = tk.Scale(display2, from_=0, to=5000, orient=tk.HORIZONTAL,
                     command=lambda x: plotScale.set(
                         500 * (int(x)//500)) if int(x) < bufferMap.get() else plotScale.set(int(bufferMap.get())))
plotScale.grid(row=4, column=1, sticky=tk.W + tk.E)

'''
luFilterMap = tk.Listbox(display2, selectmode='multiple', height=5)
luFilterMap.grid(row=4+1, column=1)
[luFilterMap.insert(i, el) for i, el in enumerate(['URB', 'CRO', 'GRA', 'DF', 'EF'])]
luFilterMap.bind('<<ListboxSelect>>', lambda e: luChoice([e.widget.get(i) for i in e.widget.curselection()]) \
    if e.widget.curselection() else None)
'''
'''
luscr = [luFilterMap.get(i) for i in luFilterMap.curselection()] if len(
    luFilterMap.curselection()) > 0 else False
'''

"""
Choose Map LU Filter
"""
frameFMap = tk.Frame(display2)
frameFMap.grid(row=5+1, column=1)

luscr = [] 
dLUimg = {'URB': {'file': 'city.png'}, 
          'CRO': {'file': 'farm.png'},
          'GRA': {'file': 'hill.png'},
          'DF': {'file': 'park.png'}, 
          'EF': {'file': 'pine-tree.png'}}
for i, (lu_, in_) in enumerate(dLUimg.items()):
    luIcon = PIL.Image.open(
        'data/pub/static/land use icons/' + in_['file']).resize((50, 50))
    luIcon_on = PIL.ImageTk.PhotoImage(luIcon, master=frameFMap)
    luIcon_off = PIL.ImageTk.PhotoImage(luIcon.convert('L'), master=frameFMap)
    dLUimg[lu_]['tkBt'] = tk.Button(frameFMap, text=lu_, image=luIcon_on, borderwidth=0,
                                    command=lambda lu_=lu_: luChoice(lu_))
    dLUimg[lu_]['tkBt'].image = luIcon_on
    dLUimg[lu_]['tkBt'].image_off = luIcon_off
    dLUimg[lu_]['tkBt'].grid(column=i, row=0)
    dLUimg[lu_]['tkImg'] = tk.Label(frameFMap, text=lu_)
    dLUimg[lu_]['tkImg'].grid(column=i, row=1)

luLabel = tk.Label(display2, text='')
luLabel.grid(row=5+1, column=0, sticky=tk.E)

tk.Label(display2, text='icons from flaticon.com').grid(
    row=6+1, column=1, sticky=tk.E)

"""
Choose Map
"""
tk.Label(display2, text='Plot Map: ').grid(row=8, column=0)

frameRMap = tk.Frame(display2)
frameRMap.grid(row=8, column=1)

rmap2plot = tk.StringVar()
radiomap = [tk.Radiobutton(frameRMap, text=el, value=el, variable=rmap2plot, command=viewSelected).grid(column=i+1, row=0)
 for i, el in enumerate(['land use', 'ndvi', 'evi', 'lswi', 'rgb', 'online'])]


"""
Menu Specific Options
"""
##display3_black
display3.pack(expand=1, fill=tk.BOTH)
#display3.grid(column=0, row=2, sticky=tk.N + tk.S + tk.W + tk.E)
#display3.grid_propagate(0)


notebook = ttk.Notebook(display3)
notebook.pack(expand=1, fill=tk.BOTH, padx=10, pady=10)

"""
Options Real data

frameRD = tk.Frame(notebook)
notebook.add(frameRD, text='Real data')
frameRD.grid_columnconfigure(1, weight=1)
"""
"""
Options Footprint
"""
frameFP = tk.Frame(notebook)
notebook.add(frameFP, text='Footprint', sticky=tk.N + tk.W + tk.E)

shellframeFP = tktt.VerticalScrolledFrame(frameFP)
shellframeFP.pack(expand=1, fill=tk.BOTH)
innerframeFP = shellframeFP.interior
innerframeFP.grid_columnconfigure(1, weight=1)

"""
innercanvasFP = tk.Canvas(frameFP)
innerframeFP = tk.Frame(innercanvasFP)
#innerframeFP.pack(expand=1, fill=tk.X)

frameFPsb = tk.Scrollbar(frameFP, command=innercanvasFP.yview)
innercanvasFP.configure(yscrollcommand=frameFPsb.set)
frameFPsb.grid(row=0, column=3, sticky=tk.N + tk.S + tk.E)

frameFPsb.pack(side="right", fill="y")
innercanvasFP.pack(fill=tk.X)
innercanvasFP.create_window(
    (0, 0), window=innerframeFP, anchor='nw', width=400)
innerframeFP.bind("<Configure>", lambda e: innercanvasFP.configure(
    scrollregion=innercanvasFP.bbox("all")))
innerframeFP.grid_columnconfigure(1, weight=1)

"""
FPactivate = tk.IntVar()
tk.Checkbutton(innerframeFP, text="Calculate footprint.", variable=FPactivate,
               onvalue=1, offvalue=0).grid(row=0, column=1, sticky=tk.W)
FPinput_N = tk.Scale(innerframeFP, from_=1, to=48,
                     orient=tk.HORIZONTAL)
FPinput_N.grid(row=0, column=1, sticky=tk.E)

tk.Label(innerframeFP, text='Zm (m): ').grid(row=1, column=0)
FPinput_Zm = tk.Scale(innerframeFP, from_=2, to=120,
                      orient=tk.HORIZONTAL)
FPinput_Zm.grid(row=1, column=1, sticky=tk.W + tk.E)

tk.Label(innerframeFP, text='OL (m): ').grid(row=2, column=0)
FPinput_OL = tk.Scale(innerframeFP, from_=-100, to=100, orient=tk.HORIZONTAL)
FPinput_OL.set(-42)
FPinput_OL.grid(row=2, column=1, sticky=tk.W + tk.E)

tk.Label(innerframeFP, text='σv (m/s): ').grid(row=3, column=0)
FPinput_SV = tk.Scale(innerframeFP, from_=0.1, to=1,
                      orient=tk.HORIZONTAL, resolution=0.01)
FPinput_SV.set(1)
FPinput_SV.grid(row=3, column=1, sticky=tk.W + tk.E)

tk.Label(innerframeFP, text='U* (m/s): ').grid(row=4, column=0)
FPinput_US = tk.Scale(innerframeFP, from_=0.2, to=1,
                      orient=tk.HORIZONTAL, resolution=0.01)
FPinput_US.set(0.4)
FPinput_US.grid(row=4, column=1, sticky=tk.W + tk.E)

tk.Label(innerframeFP, text='WD (°): ').grid(row=5, column=0)
FPinput_WD = tk.Scale(innerframeFP, from_=0, to=359,
                      orient=tk.HORIZONTAL)
FPinput_WD.grid(row=5, column=1, sticky=tk.W + tk.E)

tk.Label(innerframeFP, text='H (m): ').grid(row=6, column=0)
FPinput_H = tk.Scale(innerframeFP, from_=1000, to=1000,
                      orient=tk.HORIZONTAL)
FPinput_H.grid(row=6, column=1, sticky=tk.W + tk.E)

"""
Options VPRM
"""
frameVPRM = tk.Frame(notebook)
notebook.add(frameVPRM, text='VPRM', sticky=tk.N + tk.W + tk.E)

shellframeVPRM = tktt.VerticalScrolledFrame(frameVPRM)
shellframeVPRM.pack(expand=1, fill=tk.BOTH)

innerframeVPRM = shellframeVPRM.interior

innerframeVPRM0 = tk.Frame(innerframeVPRM)
innerframeVPRM0.pack(fill=tk.X)
innerframeVPRM0.grid_columnconfigure(1, weight=1)


tk.Label(innerframeVPRM0, text='PAR (m): ').grid(row=2, column=0)
VPRMinput_PAR = tk.Scale(innerframeVPRM0, from_=0,
                         to=1500, orient=tk.HORIZONTAL)
VPRMinput_PAR.set(1200)
VPRMinput_PAR.grid(row=2, column=1, sticky=tk.W + tk.E)

tk.Label(innerframeVPRM0, text='T (°C): ').grid(row=3, column=0)
VPRMinput_TA = tk.Scale(innerframeVPRM0, from_=-5, to=40,
                        orient=tk.HORIZONTAL)
VPRMinput_TA.set(22)
VPRMinput_TA.grid(row=3, column=1, sticky=tk.W + tk.E)

tk.Label(innerframeVPRM0, text='PAR0: ').grid(row=4, column=0)
VPRMinput_PAR0 = tk.Scale(innerframeVPRM0, from_=0, to=1500,
                      orient=tk.HORIZONTAL)
VPRMinput_PAR0.set(1132)
VPRMinput_PAR0.grid(row=4, column=1, sticky=tk.W + tk.E)

innerframeVPRM1 = tk.Frame(innerframeVPRM)
innerframeVPRM1.pack(expand=1, fill=tk.X)
[innerframeVPRM1.grid_columnconfigure(i, weight=1) for i in range(4)]

VPRMinput_Tlow = tk.Scale(innerframeVPRM1, label='Tlow (°C)', from_=-5, to=40,
                        orient=tk.HORIZONTAL)
VPRMinput_Tlow.set(2)
VPRMinput_Tlow.grid(row=0, column=0, sticky=tk.W + tk.E)

VPRMinput_Tmax = tk.Scale(innerframeVPRM1, label='Tmax (°C)', from_=-5, to=40,
                          orient=tk.HORIZONTAL)
VPRMinput_Tmax.set(40)
VPRMinput_Tmax.grid(row=0, column=1, sticky=tk.W + tk.E)

VPRMinput_Tmin = tk.Scale(innerframeVPRM1, label='Tmin (°C)', from_=-5, to=40,
                          orient=tk.HORIZONTAL)
VPRMinput_Tmin.set(0)
VPRMinput_Tmin.grid(row=0, column=2, sticky=tk.W + tk.E)

VPRMinput_Topt = tk.Scale(innerframeVPRM1, label='Topt (°C)', from_=-5, to=40,
                          orient=tk.HORIZONTAL)
VPRMinput_Topt.set(22)
VPRMinput_Topt.grid(row=0, column=3, sticky=tk.W + tk.E)

innerframeVPRM2 = tk.Frame(innerframeVPRM)
innerframeVPRM2.pack(expand=1, fill=tk.X)
[innerframeVPRM2.grid_columnconfigure(i, weight=1) for i in range(3)]

VPRMinput_λ = tk.Scale(innerframeVPRM2, label='λ (-):', from_=0, to=1,
                       orient=tk.HORIZONTAL, resolution=0.01)
VPRMinput_λ.set(0.09)
VPRMinput_λ.grid(row=0, column=0, sticky=tk.W + tk.E)

VPRMinput_α = tk.Scale(innerframeVPRM2, label='α (-):', from_=0, to=1,
                       orient=tk.HORIZONTAL, resolution=0.01)
VPRMinput_α.set(0.09)
VPRMinput_α.grid(row=0, column=1, sticky=tk.W + tk.E)

VPRMinput_β = tk.Scale(innerframeVPRM2, label='β (-):', from_=-2, to=2,
                       orient=tk.HORIZONTAL, resolution=0.1)
VPRMinput_β.set(0.3)
VPRMinput_β.grid(row=0, column=2, sticky=tk.W + tk.E)

innerframeVPRM3 = tk.Frame(innerframeVPRM)
innerframeVPRM3.pack(pady=10)
tk.Button(innerframeVPRM3, text='Calculate VPRM',
          command=viewVPRM).pack(side='left')

def winVPRMinfo():
    new_window()
    nwind.title ('VPRM Equations')
    tk.Label(nwind, text='NEE = GPP + Reco\n' +
             'GPP = - (λ * Tscale * Pscale * Wscale) * (1 / (1 + PAR/PAR0)) * EVI * PAR\n' +
             'Reco = α*T  + β\n' +
             'Tscale = (T-Tmin) * (T-Tmax) / (((T - Tmin) * (T - Tmax)) - (T - Topt)**2)\n' +
             'Pscale = (1 + LSWI) / 2\n' +
             'Wscale=(1 + LSWI) / (1 + max(LSWI))', justify='left').pack()


VPRMinfo = tk.Button(innerframeVPRM3, text='+ info', command=winVPRMinfo)
VPRMinfo.pack(side='right')

lastx, lasty = None, None

##display5_purple
#display5.pack(fill= tk.X, side='right')
#display5.place(x=0, y=250, relwidth=0.5, height=20)
#display5.grid_propagate(0)


previewFrame = tk.Frame(previewframe)
previewFrame.pack(expand=1, fill=tk.BOTH)

root.mainloop()

